package com.hbut603.yuhewenapi.api.payment;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.hbut603.yuhewenapi.api.utils.MyAlipayConstants;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/pay")
public class AliPay {
    @ApiOperation("订单支付")
    @GetMapping("/alipay")
    public Object payment(
            //商户订单号
            @RequestParam(value = "outtradeno") String outTradeNo,
            //总订单金额
            @RequestParam(value = "totalamount") String totalAmount,
            //订单标题
            @RequestParam(value = "subject") String subject,
            //订单描述
            @RequestParam(value = "body") String body) {
        String URL = MyAlipayConstants.APP_GATEWAY;
        String APP_ID = MyAlipayConstants.APP_ID;
        String APP_PRIVATE = MyAlipayConstants.APP_PRIVATE_KEY;
        String APP_PUBLIC = MyAlipayConstants.APP_PUBLIC_KEY;
        String TIMEOUT = MyAlipayConstants.TIMEOUT_EXPRESS;
        AlipayClient alipayClient = new DefaultAlipayClient(URL, APP_ID, APP_PRIVATE, "json", "UTF-8", APP_PUBLIC, "RSA2");
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setBizContent(
                "{\"out_trade_no\":\"" + outTradeNo + "\","
                        + "\"total_amount\":\"" + totalAmount + "\","
                        + "\"subject\":\"" + subject + "\","
                        + "\"timeout_express\":\"" + TIMEOUT + "\","
                        + "\"body\":\"" + body + "\","
                        + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        String form = "";
        try {
            form = alipayClient.pageExecute(alipayRequest).getBody();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return form;
    }
}
