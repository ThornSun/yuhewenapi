package com.hbut603.yuhewenapi.api.mapper;

import com.hbut603.yuhewenapi.api.entity.Remarks;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评语表 Mapper 接口
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface RemarksMapper extends BaseMapper<Remarks> {

}
