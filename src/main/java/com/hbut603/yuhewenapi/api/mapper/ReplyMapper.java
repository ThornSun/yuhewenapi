package com.hbut603.yuhewenapi.api.mapper;

import com.hbut603.yuhewenapi.api.entity.Reply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 回复表 Mapper 接口
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface ReplyMapper extends BaseMapper<Reply> {

}
