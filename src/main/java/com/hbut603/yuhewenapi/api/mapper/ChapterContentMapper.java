package com.hbut603.yuhewenapi.api.mapper;

import com.hbut603.yuhewenapi.api.entity.ChapterContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 章节内容表 Mapper 接口
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface ChapterContentMapper extends BaseMapper<ChapterContent> {

}
