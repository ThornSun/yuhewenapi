package com.hbut603.yuhewenapi.api.mapper;

import com.hbut603.yuhewenapi.api.entity.Examination;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 试题表 Mapper 接口
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface ExaminationMapper extends BaseMapper<Examination> {

}
