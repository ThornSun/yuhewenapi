package com.hbut603.yuhewenapi.api.mapper;

import com.hbut603.yuhewenapi.api.entity.Orders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface OrdersMapper extends BaseMapper<Orders> {

}
