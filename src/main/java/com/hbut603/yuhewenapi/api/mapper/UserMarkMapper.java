package com.hbut603.yuhewenapi.api.mapper;

import com.hbut603.yuhewenapi.api.entity.UserMark;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户评测成绩表 Mapper 接口
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface UserMarkMapper extends BaseMapper<UserMark> {

}
