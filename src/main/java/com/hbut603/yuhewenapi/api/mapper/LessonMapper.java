package com.hbut603.yuhewenapi.api.mapper;

import com.hbut603.yuhewenapi.api.entity.Lesson;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程表 Mapper 接口
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface LessonMapper extends BaseMapper<Lesson> {

}
