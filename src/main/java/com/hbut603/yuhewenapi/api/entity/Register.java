package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value="Register对象", description="注册信息表")
public class Register extends Model<Register> {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "协议")
    private boolean Agreement;

    @ApiModelProperty(value = "验证码")
    private String Captcha;

    @ApiModelProperty(value = "验证结果")
    private String Confirm;

    @ApiModelProperty(value = "邮箱")
    private String Mail;

    @ApiModelProperty(value = "真实姓名")
    private String Name;

    @ApiModelProperty(value = "密码")
    private String Passwords;

    @ApiModelProperty(value = "前缀")
    private String Prefix;

    @ApiModelProperty(value = "电话号码")
    private String Tel;

    @ApiModelProperty(value = "用户名")
    private String Username;

    public boolean isAgreement() {
        return Agreement;
    }

    public void setAgreement(boolean agreement) {
        Agreement = agreement;
    }

    public String getCaptcha() {
        return Captcha;
    }

    public void setCaptcha(String captcha) {
        Captcha = captcha;
    }

    public String getConfirm() {
        return Confirm;
    }

    public void setConfirm(String confirm) {
        Confirm = confirm;
    }

    public String getMail() {
        return Mail;
    }

    public void setMail(String mail) {
        Mail = mail;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPasswords() {
        return Passwords;
    }

    public void setPasswords(String passwords) {
        Passwords = passwords;
    }

    public String getPrefix() {
        return Prefix;
    }

    public void setPrefix(String prefix) {
        Prefix = prefix;
    }

    public String getTel() {
        return Tel;
    }

    public void setTel(String tel) {
        Tel = tel;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    @Override
    protected Serializable pkVal() {
        return Tel;
    }

    @Override
    public String toString() {
        return "Register{" +
                "Agreement=" + Agreement +
                ", Captcha='" + Captcha + '\'' +
                ", Confirm='" + Confirm + '\'' +
                ", Mail='" + Mail + '\'' +
                ", Name='" + Name + '\'' +
                ", Passwords='" + Passwords + '\'' +
                ", Prefix='" + Prefix + '\'' +
                ", Tel='" + Tel + '\'' +
                ", Username='" + Username + '\'' +
                '}';
    }
}
