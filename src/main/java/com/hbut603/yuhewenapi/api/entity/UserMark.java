package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 用户评测成绩表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="UserMark对象", description="用户评测成绩表")
public class UserMark extends Model<UserMark> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "mark_id", type = IdType.AUTO)
    private Integer markId;

    @ApiModelProperty(value = "测试编号")
    private String markCode;

    @ApiModelProperty(value = "测试用户")
    private String markUser;

    @ApiModelProperty(value = "测试分数")
    private String markScore;

    @ApiModelProperty(value = "测试时间")
    private String markCreateTime;

    @ApiModelProperty(value = "测试评语")
    private Integer markRemarks;


    public Integer getMarkId() {
        return markId;
    }

    public void setMarkId(Integer markId) {
        this.markId = markId;
    }

    public String getMarkCode() {
        return markCode;
    }

    public void setMarkCode(String markCode) {
        this.markCode = markCode;
    }

    public String getMarkUser() {
        return markUser;
    }

    public void setMarkUser(String markUser) {
        this.markUser = markUser;
    }

    public String getMarkScore() {
        return markScore;
    }

    public void setMarkScore(String markScore) {
        this.markScore = markScore;
    }

    public String getMarkCreateTime() {
        return markCreateTime;
    }

    public void setMarkCreateTime(String markCreateTime) {
        this.markCreateTime = markCreateTime;
    }

    public Integer getMarkRemarks() {
        return markRemarks;
    }

    public void setMarkRemarks(Integer markRemarks) {
        this.markRemarks = markRemarks;
    }

    @Override
    protected Serializable pkVal() {
        return this.markId;
    }

    @Override
    public String toString() {
        return "UserMark{" +
        "markId=" + markId +
        ", markCode=" + markCode +
        ", markUser=" + markUser +
        ", markScore=" + markScore +
        ", markCreateTime=" + markCreateTime +
        ", markRemarks=" + markRemarks +
        "}";
    }
}
