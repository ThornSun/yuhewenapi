package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 提问表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="Question对象", description="提问表")
public class Question extends Model<Question> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "question_id", type = IdType.AUTO)
    private Integer questionId;

    @ApiModelProperty(value = "问题编号")
    private String questionCode;

    @ApiModelProperty(value = "标题")
    private String questionTitle;

    @ApiModelProperty(value = "内容")
    private String questionContent;

    @ApiModelProperty(value = "提问人")
    private String questionUser;

    @ApiModelProperty(value = "提问类型")
    private String untitled;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime questionCreatTime;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime questionUpdateTime;

    @ApiModelProperty(value = "是否公开接受回答")
    private Integer questionIsopen;


    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getQuestionCode() {
        return questionCode;
    }

    public void setQuestionCode(String questionCode) {
        this.questionCode = questionCode;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public String getQuestionUser() {
        return questionUser;
    }

    public void setQuestionUser(String questionUser) {
        this.questionUser = questionUser;
    }

    public String getUntitled() {
        return untitled;
    }

    public void setUntitled(String untitled) {
        this.untitled = untitled;
    }

    public LocalDateTime getQuestionCreatTime() {
        return questionCreatTime;
    }

    public void setQuestionCreatTime(LocalDateTime questionCreatTime) {
        this.questionCreatTime = questionCreatTime;
    }

    public LocalDateTime getQuestionUpdateTime() {
        return questionUpdateTime;
    }

    public void setQuestionUpdateTime(LocalDateTime questionUpdateTime) {
        this.questionUpdateTime = questionUpdateTime;
    }

    public Integer getQuestionIsopen() {
        return questionIsopen;
    }

    public void setQuestionIsopen(Integer questionIsopen) {
        this.questionIsopen = questionIsopen;
    }

    @Override
    protected Serializable pkVal() {
        return this.questionId;
    }

    @Override
    public String toString() {
        return "Question{" +
        "questionId=" + questionId +
        ", questionCode=" + questionCode +
        ", questionTitle=" + questionTitle +
        ", questionContent=" + questionContent +
        ", questionUser=" + questionUser +
        ", untitled=" + untitled +
        ", questionCreatTime=" + questionCreatTime +
        ", questionUpdateTime=" + questionUpdateTime +
        ", questionIsopen=" + questionIsopen +
        "}";
    }
}
