package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 评论表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="Comment对象", description="评论表")
public class Comment extends Model<Comment> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "comment_id", type = IdType.AUTO)
    private Integer commentId;

    @ApiModelProperty(value = "评论编号")
    private String commentCode;

    @ApiModelProperty(value = "评论目标")
    private String commentTarget;

    @ApiModelProperty(value = "评论内容")
    private String commentContent;

    @ApiModelProperty(value = "评论用户")
    private String commentUser;

    @ApiModelProperty(value = "评论时间")
    private String commentCreateTime;

    @ApiModelProperty(value = "更新时间")
    private String commentUpdateTime;


    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public String getCommentCode() {
        return commentCode;
    }

    public void setCommentCode(String commentCode) {
        this.commentCode = commentCode;
    }

    public String getCommentTarget() {
        return commentTarget;
    }

    public void setCommentTarget(String commentTarget) {
        this.commentTarget = commentTarget;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public String getCommentUser() {
        return commentUser;
    }

    public void setCommentUser(String commentUser) {
        this.commentUser = commentUser;
    }

    public String getCommentCreateTime() {
        return commentCreateTime;
    }

    public void setCommentCreateTime(String commentCreateTime) {
        this.commentCreateTime = commentCreateTime;
    }

    public String getCommentUpdateTime() {
        return commentUpdateTime;
    }

    public void setCommentUpdateTime(String commentUpdateTime) {
        this.commentUpdateTime = commentUpdateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.commentId;
    }

    @Override
    public String toString() {
        return "Comment{" +
        "commentId=" + commentId +
        ", commentCode=" + commentCode +
        ", commentTarget=" + commentTarget +
        ", commentContent=" + commentContent +
        ", commentUser=" + commentUser +
        ", commentCreateTime=" + commentCreateTime +
        ", commentUpdateTime=" + commentUpdateTime +
        "}";
    }
}
