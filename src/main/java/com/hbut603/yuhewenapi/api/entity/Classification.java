package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 分类表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="Classification对象", description="分类表")
public class Classification extends Model<Classification> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "class_id", type = IdType.AUTO)
    private Integer classId;

    @ApiModelProperty(value = "商品类型 视频、文件、套餐")
    private Integer classType;

    @ApiModelProperty(value = "课程授课方式 直播、视频")
    private Integer classTeachType;

    @ApiModelProperty(value = "适用年级")
    private Integer classGrade;

    @ApiModelProperty(value = "适用课程 阅读、写作等")
    private Integer classLesson;


    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getClassType() {
        return classType;
    }

    public void setClassType(Integer classType) {
        this.classType = classType;
    }

    public Integer getClassTeachType() {
        return classTeachType;
    }

    public void setClassTeachType(Integer classTeachType) {
        this.classTeachType = classTeachType;
    }

    public Integer getClassGrade() {
        return classGrade;
    }

    public void setClassGrade(Integer classGrade) {
        this.classGrade = classGrade;
    }

    public Integer getClassLesson() {
        return classLesson;
    }

    public void setClassLesson(Integer classLesson) {
        this.classLesson = classLesson;
    }

    @Override
    protected Serializable pkVal() {
        return this.classId;
    }

    @Override
    public String toString() {
        return "Classification{" +
        "classId=" + classId +
        ", classType=" + classType +
        ", classTeachType=" + classTeachType +
        ", classGrade=" + classGrade +
        ", classLesson=" + classLesson +
        "}";
    }
}
