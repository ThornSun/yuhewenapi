package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="Orders对象", description="订单表")
public class Orders extends Model<Orders> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "order_id", type = IdType.AUTO)
    private Integer orderId;

    @ApiModelProperty(value = "订单编号")
    private String orderCode;

    @ApiModelProperty(value = "订单状态")
    private String orderState;

    @ApiModelProperty(value = "商品数量")
    private String orderQuantity;

    @ApiModelProperty(value = "商品总价")
    private String orderTotalPrice;

    @ApiModelProperty(value = "订单金额 实付金额")
    private String orderAmountPaid;

    @ApiModelProperty(value = "订单支付渠道")
    private String orderPaidChannel;

    @ApiModelProperty(value = "订单支付流水号")
    private String orderSerialNumber;

    @ApiModelProperty(value = "订单创建时间")
    private LocalDateTime orderCreateTime;

    @ApiModelProperty(value = "订单付款时间")
    private LocalDateTime orderPaidTime;

    @ApiModelProperty(value = "订单完成时间")
    private LocalDateTime orderFinishedTime;

    @ApiModelProperty(value = "客户编号")
    private String orderUserId;

    @ApiModelProperty(value = "客户备注")
    private String orderTips;


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getOrderState() {
        return orderState;
    }

    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }

    public String getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(String orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public String getOrderTotalPrice() {
        return orderTotalPrice;
    }

    public void setOrderTotalPrice(String orderTotalPrice) {
        this.orderTotalPrice = orderTotalPrice;
    }

    public String getOrderAmountPaid() {
        return orderAmountPaid;
    }

    public void setOrderAmountPaid(String orderAmountPaid) {
        this.orderAmountPaid = orderAmountPaid;
    }

    public String getOrderPaidChannel() {
        return orderPaidChannel;
    }

    public void setOrderPaidChannel(String orderPaidChannel) {
        this.orderPaidChannel = orderPaidChannel;
    }

    public String getOrderSerialNumber() {
        return orderSerialNumber;
    }

    public void setOrderSerialNumber(String orderSerialNumber) {
        this.orderSerialNumber = orderSerialNumber;
    }

    public LocalDateTime getOrderCreateTime() {
        return orderCreateTime;
    }

    public void setOrderCreateTime(LocalDateTime orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }

    public LocalDateTime getOrderPaidTime() {
        return orderPaidTime;
    }

    public void setOrderPaidTime(LocalDateTime orderPaidTime) {
        this.orderPaidTime = orderPaidTime;
    }

    public LocalDateTime getOrderFinishedTime() {
        return orderFinishedTime;
    }

    public void setOrderFinishedTime(LocalDateTime orderFinishedTime) {
        this.orderFinishedTime = orderFinishedTime;
    }

    public String getOrderUserId() {
        return orderUserId;
    }

    public void setOrderUserId(String orderUserId) {
        this.orderUserId = orderUserId;
    }

    public String getOrderTips() {
        return orderTips;
    }

    public void setOrderTips(String orderTips) {
        this.orderTips = orderTips;
    }

    @Override
    protected Serializable pkVal() {
        return this.orderId;
    }

    @Override
    public String toString() {
        return "Orders{" +
        "orderId=" + orderId +
        ", orderCode=" + orderCode +
        ", orderState=" + orderState +
        ", orderQuantity=" + orderQuantity +
        ", orderTotalPrice=" + orderTotalPrice +
        ", orderAmountPaid=" + orderAmountPaid +
        ", orderPaidChannel=" + orderPaidChannel +
        ", orderSerialNumber=" + orderSerialNumber +
        ", orderCreateTime=" + orderCreateTime +
        ", orderPaidTime=" + orderPaidTime +
        ", orderFinishedTime=" + orderFinishedTime +
        ", orderUserId=" + orderUserId +
        ", orderTips=" + orderTips +
        "}";
    }
}
