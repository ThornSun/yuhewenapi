package com.hbut603.yuhewenapi.api.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 优惠活动表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="OffersActivities对象", description="优惠活动表")
public class OffersActivities extends Model<OffersActivities> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "oa_id", type = IdType.AUTO)
    private Integer oaId;

    @ApiModelProperty(value = "活动代码")
    private String oaCode;

    @ApiModelProperty(value = "活动标题")
    private String oaTitle;

    @ApiModelProperty(value = "活动介绍")
    private String oaIntro;

    @ApiModelProperty(value = "活动内容")
    private String oaContent;

    @ApiModelProperty(value = "参与商品 参与活动商品id")
    private String oaProducts;

    @ApiModelProperty(value = "商品原价 参与活动商品价格总和")
    private BigDecimal oaOriginalPrice;

    @ApiModelProperty(value = "活动价格")
    private BigDecimal oaDiscountPrice;

    @ApiModelProperty(value = "活动开始时间")
    private LocalDateTime oaStartTime;

    @ApiModelProperty(value = "活动结束时间")
    private LocalDateTime oaEndTime;

    @ApiModelProperty(value = "是否限额 0：不限，1：限额")
    private Integer oaIslimit;

    @ApiModelProperty(value = "活动份额")
    private Integer oaQuantity;

    @ApiModelProperty(value = "是否会员专享 0：不是，1：是")
    private Integer oaIsviponly;


    public Integer getOaId() {
        return oaId;
    }

    public void setOaId(Integer oaId) {
        this.oaId = oaId;
    }

    public String getOaCode() {
        return oaCode;
    }

    public void setOaCode(String oaCode) {
        this.oaCode = oaCode;
    }

    public String getOaTitle() {
        return oaTitle;
    }

    public void setOaTitle(String oaTitle) {
        this.oaTitle = oaTitle;
    }

    public String getOaIntro() {
        return oaIntro;
    }

    public void setOaIntro(String oaIntro) {
        this.oaIntro = oaIntro;
    }

    public String getOaContent() {
        return oaContent;
    }

    public void setOaContent(String oaContent) {
        this.oaContent = oaContent;
    }

    public String getOaProducts() {
        return oaProducts;
    }

    public void setOaProducts(String oaProducts) {
        this.oaProducts = oaProducts;
    }

    public BigDecimal getOaOriginalPrice() {
        return oaOriginalPrice;
    }

    public void setOaOriginalPrice(BigDecimal oaOriginalPrice) {
        this.oaOriginalPrice = oaOriginalPrice;
    }

    public BigDecimal getOaDiscountPrice() {
        return oaDiscountPrice;
    }

    public void setOaDiscountPrice(BigDecimal oaDiscountPrice) {
        this.oaDiscountPrice = oaDiscountPrice;
    }

    public LocalDateTime getOaStartTime() {
        return oaStartTime;
    }

    public void setOaStartTime(LocalDateTime oaStartTime) {
        this.oaStartTime = oaStartTime;
    }

    public LocalDateTime getOaEndTime() {
        return oaEndTime;
    }

    public void setOaEndTime(LocalDateTime oaEndTime) {
        this.oaEndTime = oaEndTime;
    }

    public Integer getOaIslimit() {
        return oaIslimit;
    }

    public void setOaIslimit(Integer oaIslimit) {
        this.oaIslimit = oaIslimit;
    }

    public Integer getOaQuantity() {
        return oaQuantity;
    }

    public void setOaQuantity(Integer oaQuantity) {
        this.oaQuantity = oaQuantity;
    }

    public Integer getOaIsviponly() {
        return oaIsviponly;
    }

    public void setOaIsviponly(Integer oaIsviponly) {
        this.oaIsviponly = oaIsviponly;
    }

    @Override
    protected Serializable pkVal() {
        return this.oaId;
    }

    @Override
    public String toString() {
        return "OffersActivities{" +
        "oaId=" + oaId +
        ", oaCode=" + oaCode +
        ", oaTitle=" + oaTitle +
        ", oaIntro=" + oaIntro +
        ", oaContent=" + oaContent +
        ", oaProducts=" + oaProducts +
        ", oaOriginalPrice=" + oaOriginalPrice +
        ", oaDiscountPrice=" + oaDiscountPrice +
        ", oaStartTime=" + oaStartTime +
        ", oaEndTime=" + oaEndTime +
        ", oaIslimit=" + oaIslimit +
        ", oaQuantity=" + oaQuantity +
        ", oaIsviponly=" + oaIsviponly +
        "}";
    }
}
