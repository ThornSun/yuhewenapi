package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 资源权限表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="RespRelation对象", description="资源权限表")
public class RespRelation extends Model<RespRelation> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "resp_id", type = IdType.AUTO)
    private Integer respId;

    @ApiModelProperty(value = "权限ID")
    private Integer respPerId;

    @ApiModelProperty(value = "资源ID")
    private String respResId;


    public Integer getRespId() {
        return respId;
    }

    public void setRespId(Integer respId) {
        this.respId = respId;
    }

    public Integer getRespPerId() {
        return respPerId;
    }

    public void setRespPerId(Integer respPerId) {
        this.respPerId = respPerId;
    }

    public String getRespResId() {
        return respResId;
    }

    public void setRespResId(String respResId) {
        this.respResId = respResId;
    }

    @Override
    protected Serializable pkVal() {
        return this.respId;
    }

    @Override
    public String toString() {
        return "RespRelation{" +
        "respId=" + respId +
        ", respPerId=" + respPerId +
        ", respResId=" + respResId +
        "}";
    }
}
