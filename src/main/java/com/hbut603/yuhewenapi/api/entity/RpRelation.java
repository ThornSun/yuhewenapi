package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 角色权限表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="RpRelation对象", description="角色权限表")
public class RpRelation extends Model<RpRelation> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "rp_id", type = IdType.AUTO)
    private Integer rpId;

    @ApiModelProperty(value = "角色ID")
    private Integer rpRoleId;

    @ApiModelProperty(value = "权限ID")
    private Integer rpPermissionId;


    public Integer getRpId() {
        return rpId;
    }

    public void setRpId(Integer rpId) {
        this.rpId = rpId;
    }

    public Integer getRpRoleId() {
        return rpRoleId;
    }

    public void setRpRoleId(Integer rpRoleId) {
        this.rpRoleId = rpRoleId;
    }

    public Integer getRpPermissionId() {
        return rpPermissionId;
    }

    public void setRpPermissionId(Integer rpPermissionId) {
        this.rpPermissionId = rpPermissionId;
    }

    @Override
    protected Serializable pkVal() {
        return this.rpId;
    }

    @Override
    public String toString() {
        return "RpRelation{" +
        "rpId=" + rpId +
        ", rpRoleId=" + rpRoleId +
        ", rpPermissionId=" + rpPermissionId +
        "}";
    }
}
