package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 点赞表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="Zan对象", description="点赞表")
public class Zan extends Model<Zan> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "zan_id", type = IdType.AUTO)
    private Integer zanId;

    @ApiModelProperty(value = "点赞编号")
    private String zanCode;

    @ApiModelProperty(value = "点赞目标")
    private String zanTarget;

    @ApiModelProperty(value = "点赞用户")
    private String zanUser;


    public Integer getZanId() {
        return zanId;
    }

    public void setZanId(Integer zanId) {
        this.zanId = zanId;
    }

    public String getZanCode() {
        return zanCode;
    }

    public void setZanCode(String zanCode) {
        this.zanCode = zanCode;
    }

    public String getZanTarget() {
        return zanTarget;
    }

    public void setZanTarget(String zanTarget) {
        this.zanTarget = zanTarget;
    }

    public String getZanUser() {
        return zanUser;
    }

    public void setZanUser(String zanUser) {
        this.zanUser = zanUser;
    }

    @Override
    protected Serializable pkVal() {
        return this.zanId;
    }

    @Override
    public String toString() {
        return "Zan{" +
        "zanId=" + zanId +
        ", zanCode=" + zanCode +
        ", zanTarget=" + zanTarget +
        ", zanUser=" + zanUser +
        "}";
    }
}
