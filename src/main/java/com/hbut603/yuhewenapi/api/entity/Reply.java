package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 回复表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="Reply对象", description="回复表")
public class Reply extends Model<Reply> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "reply_id", type = IdType.AUTO)
    private Integer replyId;

    @ApiModelProperty(value = "回复编号")
    private String replyCode;

    @ApiModelProperty(value = "回复目标")
    private String replyTarget;

    @ApiModelProperty(value = "回复内容")
    private String replyContent;

    @ApiModelProperty(value = "回复用户")
    private String replyUser;

    @ApiModelProperty(value = "回复目标用户")
    private String replyTargetUser;

    @ApiModelProperty(value = "回复时间")
    private String replyCreateTime;

    @ApiModelProperty(value = "回复更新时间")
    private String replyUpdateTime;


    public Integer getReplyId() {
        return replyId;
    }

    public void setReplyId(Integer replyId) {
        this.replyId = replyId;
    }

    public String getReplyCode() {
        return replyCode;
    }

    public void setReplyCode(String replyCode) {
        this.replyCode = replyCode;
    }

    public String getReplyTarget() {
        return replyTarget;
    }

    public void setReplyTarget(String replyTarget) {
        this.replyTarget = replyTarget;
    }

    public String getReplyContent() {
        return replyContent;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent;
    }

    public String getReplyUser() {
        return replyUser;
    }

    public void setReplyUser(String replyUser) {
        this.replyUser = replyUser;
    }

    public String getReplyTargetUser() {
        return replyTargetUser;
    }

    public void setReplyTargetUser(String replyTargetUser) {
        this.replyTargetUser = replyTargetUser;
    }

    public String getReplyCreateTime() {
        return replyCreateTime;
    }

    public void setReplyCreateTime(String replyCreateTime) {
        this.replyCreateTime = replyCreateTime;
    }

    public String getReplyUpdateTime() {
        return replyUpdateTime;
    }

    public void setReplyUpdateTime(String replyUpdateTime) {
        this.replyUpdateTime = replyUpdateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.replyId;
    }

    @Override
    public String toString() {
        return "Reply{" +
        "replyId=" + replyId +
        ", replyCode=" + replyCode +
        ", replyTarget=" + replyTarget +
        ", replyContent=" + replyContent +
        ", replyUser=" + replyUser +
        ", replyTargetUser=" + replyTargetUser +
        ", replyCreateTime=" + replyCreateTime +
        ", replyUpdateTime=" + replyUpdateTime +
        "}";
    }
}
