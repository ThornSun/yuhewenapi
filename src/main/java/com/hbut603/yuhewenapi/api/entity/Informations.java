package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 资讯与动态表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="Informations对象", description="资讯与动态表")
public class Informations extends Model<Informations> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "info_id", type = IdType.AUTO)
    private Integer infoId;

    @ApiModelProperty(value = "资讯编号")
    private String infoCode;

    @ApiModelProperty(value = "资讯标题")
    private String infoTitle;

    @ApiModelProperty(value = "资讯内容")
    private String infoContent;

    @ApiModelProperty(value = "资讯类型")
    private String infoType;

    @ApiModelProperty(value = "资讯图片")
    private String infoImage;

    @ApiModelProperty(value = "创建时间")
    private String infoTime;


    public Integer getInfoId() {
        return infoId;
    }

    public void setInfoId(Integer infoId) {
        this.infoId = infoId;
    }

    public String getInfoCode() {
        return infoCode;
    }

    public void setInfoCode(String infoCode) {
        this.infoCode = infoCode;
    }

    public String getInfoTitle() {
        return infoTitle;
    }

    public void setInfoTitle(String infoTitle) {
        this.infoTitle = infoTitle;
    }

    public String getInfoContent() {
        return infoContent;
    }

    public void setInfoContent(String infoContent) {
        this.infoContent = infoContent;
    }

    public String getInfoType() {
        return infoType;
    }

    public void setInfoType(String infoType) {
        this.infoType = infoType;
    }

    public String getInfoImage() {
        return infoImage;
    }

    public void setInfoImage(String infoImage) {
        this.infoImage = infoImage;
    }

    public String getInfoTime() {
        return infoTime;
    }

    public void setInfoTime(String infoTime) {
        this.infoTime = infoTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.infoId;
    }

    @Override
    public String toString() {
        return "Informations{" +
        "infoId=" + infoId +
        ", infoCode=" + infoCode +
        ", infoTitle=" + infoTitle +
        ", infoContent=" + infoContent +
        ", infoType=" + infoType +
        ", infoImage=" + infoImage +
        ", infoTime=" + infoTime +
        "}";
    }
}
