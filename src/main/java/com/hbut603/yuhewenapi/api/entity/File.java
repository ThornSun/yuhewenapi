package com.hbut603.yuhewenapi.api.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 文件资料表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="File对象", description="文件资料表")
public class File extends Model<File> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "file_id", type = IdType.AUTO)
    private Integer fileId;

    @ApiModelProperty(value = "资料编号")
    private String fileCode;

    @ApiModelProperty(value = "资料名称")
    private String fileName;

    @ApiModelProperty(value = "资料介绍")
    private String fileIntro;

    @ApiModelProperty(value = "资料价格")
    private BigDecimal filePrice;

    @ApiModelProperty(value = "下载链接")
    private String fileLink;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime fileCreateTime;
    
    @ApiModelProperty(value = "资料封面")
    private String fileImage;

    public Integer getFileId() {
        return fileId;
    }

    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileIntro() {
        return fileIntro;
    }

    public void setFileIntro(String fileIntro) {
        this.fileIntro = fileIntro;
    }

    public BigDecimal getFilePrice() {
        return filePrice;
    }

    public void setFilePrice(BigDecimal filePrice) {
        this.filePrice = filePrice;
    }

    public String getFileLink() {
        return fileLink;
    }

    public void setFileLink(String fileLink) {
        this.fileLink = fileLink;
    }
    
    public String getFileImage() {
        return fileImage;
    }

    public void setFileImage(String fileImage) {
        this.fileImage = fileImage;
    }
    
    public LocalDateTime getFileCreateTime() {
        return fileCreateTime;
    }

    public void setFileCreateTime(LocalDateTime fileCreateTime) {
        this.fileCreateTime = fileCreateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.fileId;
    }

    @Override
    public String toString() {
        return "File{" +
        "fileId=" + fileId +
        ", fileCode=" + fileCode +
        ", fileName=" + fileName +
        ", fileIntro=" + fileIntro +
        ", filePrice=" + filePrice +
        ", fileLink=" + fileLink +
        ", fileCreateTime=" + fileCreateTime +
        ", fileImage=" + fileImage +
        "}";
    }
}
