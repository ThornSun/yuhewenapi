package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.ShoppingCart;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 购物车 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IShoppingCartService extends IService<ShoppingCart> {

}
