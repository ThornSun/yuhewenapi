package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.OffersActivities;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 优惠活动表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IOffersActivitiesService extends IService<OffersActivities> {

}
