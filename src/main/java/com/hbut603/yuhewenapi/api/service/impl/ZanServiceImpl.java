package com.hbut603.yuhewenapi.api.service.impl;

import com.hbut603.yuhewenapi.api.entity.Zan;
import com.hbut603.yuhewenapi.api.mapper.ZanMapper;
import com.hbut603.yuhewenapi.api.service.IZanService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 点赞表 服务实现类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@Service
public class ZanServiceImpl extends ServiceImpl<ZanMapper, Zan> implements IZanService {

}
