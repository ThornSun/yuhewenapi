package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IUserService extends IService<User> {

}
