package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 页面菜单表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IMenuService extends IService<Menu> {

}
