package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.UrRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IUrRelationService extends IService<UrRelation> {

}
