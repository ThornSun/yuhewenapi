package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.Classification;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 分类表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IClassificationService extends IService<Classification> {

}
