package com.hbut603.yuhewenapi.api.service.impl;

import com.hbut603.yuhewenapi.api.entity.UbrRelation;
import com.hbut603.yuhewenapi.api.mapper.UbrRelationMapper;
import com.hbut603.yuhewenapi.api.service.IUbrRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户购买资源表 服务实现类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@Service
public class UbrRelationServiceImpl extends ServiceImpl<UbrRelationMapper, UbrRelation> implements IUbrRelationService {

}
