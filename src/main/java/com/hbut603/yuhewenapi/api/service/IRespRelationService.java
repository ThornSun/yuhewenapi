package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.RespRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 资源权限表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IRespRelationService extends IService<RespRelation> {

}
