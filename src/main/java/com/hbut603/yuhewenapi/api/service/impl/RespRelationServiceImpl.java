package com.hbut603.yuhewenapi.api.service.impl;

import com.hbut603.yuhewenapi.api.entity.RespRelation;
import com.hbut603.yuhewenapi.api.mapper.RespRelationMapper;
import com.hbut603.yuhewenapi.api.service.IRespRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资源权限表 服务实现类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@Service
public class RespRelationServiceImpl extends ServiceImpl<RespRelationMapper, RespRelation> implements IRespRelationService {

}
