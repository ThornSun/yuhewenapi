package com.hbut603.yuhewenapi.api.service.impl;

import com.hbut603.yuhewenapi.api.entity.OffersActivities;
import com.hbut603.yuhewenapi.api.mapper.OffersActivitiesMapper;
import com.hbut603.yuhewenapi.api.service.IOffersActivitiesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠活动表 服务实现类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@Service
public class OffersActivitiesServiceImpl extends ServiceImpl<OffersActivitiesMapper, OffersActivities> implements IOffersActivitiesService {

}
