package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.OrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单详情表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IOrderDetailService extends IService<OrderDetail> {

}
