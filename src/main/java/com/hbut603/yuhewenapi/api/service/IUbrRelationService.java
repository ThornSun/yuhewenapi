package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.UbrRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户购买资源表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IUbrRelationService extends IService<UbrRelation> {

}
