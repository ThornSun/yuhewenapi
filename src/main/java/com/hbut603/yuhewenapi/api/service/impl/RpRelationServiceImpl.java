package com.hbut603.yuhewenapi.api.service.impl;

import com.hbut603.yuhewenapi.api.entity.RpRelation;
import com.hbut603.yuhewenapi.api.mapper.RpRelationMapper;
import com.hbut603.yuhewenapi.api.service.IRpRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限表 服务实现类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@Service
public class RpRelationServiceImpl extends ServiceImpl<RpRelationMapper, RpRelation> implements IRpRelationService {

}
