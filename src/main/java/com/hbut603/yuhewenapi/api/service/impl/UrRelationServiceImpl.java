package com.hbut603.yuhewenapi.api.service.impl;

import com.hbut603.yuhewenapi.api.entity.UrRelation;
import com.hbut603.yuhewenapi.api.mapper.UrRelationMapper;
import com.hbut603.yuhewenapi.api.service.IUrRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@Service
public class UrRelationServiceImpl extends ServiceImpl<UrRelationMapper, UrRelation> implements IUrRelationService {

}
