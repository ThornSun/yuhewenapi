package com.hbut603.yuhewenapi.api.service.impl;

import com.hbut603.yuhewenapi.api.entity.Reply;
import com.hbut603.yuhewenapi.api.mapper.ReplyMapper;
import com.hbut603.yuhewenapi.api.service.IReplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 回复表 服务实现类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@Service
public class ReplyServiceImpl extends ServiceImpl<ReplyMapper, Reply> implements IReplyService {

}
