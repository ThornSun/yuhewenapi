package com.hbut603.yuhewenapi.api.service.impl;

import com.hbut603.yuhewenapi.api.entity.UserMark;
import com.hbut603.yuhewenapi.api.mapper.UserMarkMapper;
import com.hbut603.yuhewenapi.api.service.IUserMarkService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户评测成绩表 服务实现类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@Service
public class UserMarkServiceImpl extends ServiceImpl<UserMarkMapper, UserMark> implements IUserMarkService {

}
