package com.hbut603.yuhewenapi.api.uploader;

import com.hbut603.yuhewenapi.api.utils.MyQiniuConstants;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/api/upload")
public class ImageUploader {
    @ApiOperation("图片上传")
    @PostMapping(value = "/imageupload")
    public Object payment(@RequestParam("file") MultipartFile file){
        String ACCESSKEY = MyQiniuConstants.ACCESSKEY;
        String SECREETKEY = MyQiniuConstants.SECRETKEY;
        String BUCKET = MyQiniuConstants.BUCKET;
        String QINIU_IMAGE_DOMAIN = MyQiniuConstants.IMAGEDOMAIN;
        // 获取上传的文件的名称    
        String filename = file.getOriginalFilename();
        //修改文件名称 uuid
        String fileUUIDname = UUID.randomUUID().toString().replaceAll("-","") ;
        //获取后缀
        String prefix=filename.substring(filename.lastIndexOf(".")+1);
        //修改后完整的文件名称
        String newFileName = fileUUIDname + "." + prefix;
        String result = null;
        Auth auth = Auth.create(ACCESSKEY,SECREETKEY);
        Configuration configuration = new Configuration(Zone.autoZone());
        UploadManager uploadManager = new UploadManager(configuration);
        String upToken = auth.uploadToken(BUCKET);
        try {
            Response res = uploadManager.put(file.getBytes(),newFileName, upToken);
            if (res.isOK() && res.isJson()) {
                result =  QINIU_IMAGE_DOMAIN+res.jsonToMap().get("key");
            }
        } catch (IOException e) {
            result = e.toString();
        }
        return result;
    }
}
