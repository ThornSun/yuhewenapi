package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hbut603.yuhewenapi.api.entity.Reply;
import com.hbut603.yuhewenapi.api.service.IReplyService;
import com.hbut603.yuhewenapi.api.utils.CodeGenerator;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;

/**
 * <p>
 * 回复表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/reply")
public class ReplyController {
    @Autowired
    private IReplyService replyService;

    @ApiOperation("查询所有回复")
    @GetMapping("/findall")
    public Object findAll(
            @RequestParam("currentPage") Integer currentPage,
            @RequestParam("pageSize") Integer pageSize) {
        Page<Reply> page = new Page<>(currentPage,pageSize);
        IPage<Reply> replys = replyService.page(page.setDesc("reply_id"));
        return new JsonData().buildSuccess(replys, 200);
    }

    @ApiOperation("查询单个回复")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "R_id")  String account) {
        QueryWrapper queryWrapper = new QueryWrapper<Reply>();
        Reply reply = replyService.getOne(queryWrapper.eq(true,"reply_account",account));
        return new JsonData().buildSuccess(reply, 200);
    }

    @ApiOperation("新增回复")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody Reply reply) {
        reply.setReplyCode(new CodeGenerator().codeGenerateCenter(4));
        boolean isSuccess = replyService.save(reply);
        return new JsonData().buildSuccess(""+isSuccess,0);
    }

    @ApiOperation("修改单个回复信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个回复")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "R_id")  String account) {
        QueryWrapper queryWrapper = new QueryWrapper<Reply>();
        boolean isSuccess = replyService.remove(queryWrapper.eq(true,"reply_account",account));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }
    
    @ApiOperation("查询所有对应回复")
    @GetMapping("/findbytarget")
    public Object findByTarget(@RequestParam(value = "target")  String target) {
        QueryWrapper queryWrapper = new QueryWrapper<Reply>();
        List<Reply> reply = replyService.listObjs(queryWrapper.eq(true,"reply_target",target));
        return new JsonData().buildSuccess(reply, 200);
    }
}

