package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hbut603.yuhewenapi.api.entity.Orders;
import com.hbut603.yuhewenapi.api.service.IOrdersService;
import com.hbut603.yuhewenapi.api.utils.CodeGenerator;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/orders")
public class OrdersController {
	@Autowired
    private IOrdersService ordersService;

    @ApiOperation("查询所有订单")
    @GetMapping("/findall")
    public Object findAll(
            @RequestParam("currentPage") Integer currentPage,
            @RequestParam("pageSize") Integer pageSize) {
        Page<Orders> page = new Page<>(currentPage,pageSize);
        IPage<Orders> orders = ordersService.page(page.setDesc("order_id"));
        return new JsonData().buildSuccess(orders, 200);
    }

    @ApiOperation("查询单个订单")
    @ApiImplicitParam(value = "订单编号")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "oid")  String id) {
        QueryWrapper queryWrapper = new QueryWrapper<Orders>();
        Orders orders = ordersService.getOne(queryWrapper.eq(true,"order_id",id));
        return new JsonData().buildSuccess(orders, 200);
    }

    @ApiOperation("新增订单")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody Orders orders) {
        String code = new CodeGenerator().codeGenerateCenter(5);
        orders.setOrderCode(code);
        boolean isSuccess = ordersService.save(orders);
        return new JsonData().buildSuccess(""+isSuccess,code);
    }

    @ApiOperation("修改单个订单信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个订单")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "oid")  String id) {
        QueryWrapper queryWrapper = new QueryWrapper<Orders>();
        boolean isSuccess = ordersService.remove(queryWrapper.eq(true,"order_id",id));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }

}

