package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hbut603.yuhewenapi.api.entity.RpRelation;
import com.hbut603.yuhewenapi.api.service.IRpRelationService;
import com.hbut603.yuhewenapi.api.utils.JsonData;

import io.swagger.annotations.ApiOperation;


import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * 角色权限表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/rpRelation")
public class RpRelationController {
    @Autowired
    private IRpRelationService rpRelationService;

    @ApiOperation("查询所有角色对应权限")
    @GetMapping("/findall")
    public Object findAll(
            @RequestParam("currentPage") Integer currentPage,
            @RequestParam("pageSize") Integer pageSize) {
        Page<RpRelation> page = new Page<>(currentPage, pageSize);
        IPage<RpRelation> rpRelation = rpRelationService.page(page);
        return new JsonData().buildSuccess(rpRelation, 200);
    }

    @ApiOperation("查询单个角色权限")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "rpid") String id) {
        QueryWrapper queryWrapper = new QueryWrapper<RpRelation>();
        RpRelation rpRelation = rpRelationService.getOne(queryWrapper.eq(true, "rp_id", id));
        return new JsonData().buildSuccess(rpRelation, 200);
    }

    @ApiOperation("新增角色对应权限")
    @PostMapping(value = "/regist", consumes = "application/json")
    public Object regist(@RequestBody RpRelation rpRelation) {
        boolean isSuccess = rpRelationService.save(rpRelation);
        return new JsonData().buildSuccess("" + isSuccess, 0);
    }

    @ApiOperation("修改单个用户信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个用户")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "rpid") String id) {
        QueryWrapper queryWrapper = new QueryWrapper<RpRelation>();
        boolean isSuccess = rpRelationService.remove(queryWrapper.eq(true, "rp_id", id));
        return new JsonData().buildSuccess("" + isSuccess, 0);
    }
}

