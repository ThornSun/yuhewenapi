package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hbut603.yuhewenapi.api.entity.Role;
import com.hbut603.yuhewenapi.api.service.IRoleService;
import com.hbut603.yuhewenapi.api.utils.JsonData;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * 角色信息表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/role")
public class RoleController {
    @Autowired
    private IRoleService roleService;

    @ApiOperation("查询所有角色信息")
    @GetMapping("/findall")
    public Object findAll(
            @RequestParam("currentPage") Integer currentPage,
            @RequestParam("pageSize") Integer pageSize) {
        Page<Role> page = new Page<>(currentPage, pageSize);
        IPage<Role> roles = roleService.page(page);
        return new JsonData().buildSuccess(roles, 200);
    }

    @ApiOperation("查询单个角色信息")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "rnum") int num) {
        QueryWrapper queryWrapper = new QueryWrapper<Role>();
        Role role = roleService.getOne(queryWrapper.eq(true, "role_num", num));
        return new JsonData().buildSuccess(role, 200);
    }

    @ApiOperation("新增角色")
    @PostMapping(value = "/regist", consumes = "application/json")
    public Object regist(@RequestBody Role role) {
        boolean isSuccess = roleService.save(role);
        return new JsonData().buildSuccess("" + isSuccess, 0);
    }

    @ApiOperation("修改单个角色信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个角色")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "rnum") String num) {
        QueryWrapper queryWrapper = new QueryWrapper<Role>();
        boolean isSuccess = roleService.remove(queryWrapper.eq(true, "role_num", num));
        return new JsonData().buildSuccess("" + isSuccess, 0);
    }
}

