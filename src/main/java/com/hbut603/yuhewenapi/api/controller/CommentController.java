package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hbut603.yuhewenapi.api.entity.Comment;
import com.hbut603.yuhewenapi.api.service.ICommentService;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;

/**
 * <p>
 * 评论表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/comment")
public class CommentController {
    @Autowired
    private ICommentService commentService;

    @ApiOperation("查询所有评论")
    @GetMapping("/findall")
    public Object findAll(
            @RequestParam("currentPage") Integer currentPage,
            @RequestParam("pageSize") Integer pageSize) {
        Page<Comment> page = new Page<>(currentPage,pageSize);
        IPage<Comment> comments = commentService.page(page);
        return new JsonData().buildSuccess(comments, 200);
    }

    @ApiOperation("查询单个评论")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "C_id")  String id) {
        QueryWrapper queryWrapper = new QueryWrapper<Comment>();
        Comment comment = commentService.getOne(queryWrapper.eq(true,"comment_id",id));
        return new JsonData().buildSuccess(comment, 200);
    }

    @ApiOperation("新增评论")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody Comment comment) {
        boolean isSuccess = commentService.save(comment);
        return new JsonData().buildSuccess(""+isSuccess,0);
    }

    @ApiOperation("修改单个评论信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个评论")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "C_id")  String id) {
        QueryWrapper queryWrapper = new QueryWrapper<Comment>();
        boolean isSuccess = commentService.remove(queryWrapper.eq(true,"comment_id",id));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }
    
    @ApiOperation("查询所有对应评论")
    @GetMapping("/findbytarget")
    public Object findByTarget(@RequestParam(value = "target")  String target) {
        QueryWrapper queryWrapper = new QueryWrapper<Comment>();
        List<Comment> comment = commentService.listObjs(queryWrapper.eq(true,"comment_target",target));
        return new JsonData().buildSuccess(comment, 200);
    }

}

