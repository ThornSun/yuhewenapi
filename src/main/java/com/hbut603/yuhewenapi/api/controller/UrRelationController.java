package com.hbut603.yuhewenapi.api.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 用户角色表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@Controller
@RequestMapping("/api/urRelation")
public class UrRelationController {

}

