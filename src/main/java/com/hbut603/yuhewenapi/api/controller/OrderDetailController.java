package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hbut603.yuhewenapi.api.entity.OrderDetail;
import com.hbut603.yuhewenapi.api.entity.User;
import com.hbut603.yuhewenapi.api.service.IOrderDetailService;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 订单详情表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/orderDetail")
public class OrderDetailController {
	@Autowired
    private IOrderDetailService orderDetailService;

    @ApiOperation("查询单个订单详情")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "odcode")  String id) {
        QueryWrapper queryWrapper = new QueryWrapper<OrderDetail>();
        List<OrderDetail> orderDetail = orderDetailService.listObjs(queryWrapper.eq(true,"od_order_code",id));
        return new JsonData().buildSuccess(orderDetail, 200);
    }
    
    @ApiOperation("修改单个用户信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个用户")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "odid")  String id) {
        QueryWrapper queryWrapper = new QueryWrapper<OrderDetail>();
        boolean isSuccess = orderDetailService.remove(queryWrapper.eq(true,"od_id",id));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }
    
    @ApiOperation("新增订单详情")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody OrderDetail orderdetail) {
        boolean isSuccess = orderDetailService.save(orderdetail);
        return new JsonData().buildSuccess(""+isSuccess,0);
    }

}

