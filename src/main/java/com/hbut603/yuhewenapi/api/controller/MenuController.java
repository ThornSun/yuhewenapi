package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hbut603.yuhewenapi.api.entity.Menu;
import com.hbut603.yuhewenapi.api.service.IMenuService;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 页面菜单表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/menu")
public class MenuController {
    @Autowired
    private IMenuService menuService;

    @ApiOperation("查询所有菜单")
    @GetMapping("/findall")
    public Object findAll(
            @RequestParam("currentPage") Integer currentPage,
            @RequestParam("pageSize") Integer pageSize) {
        Page<Menu> page = new Page<>(currentPage, pageSize);
        IPage<Menu> menu = menuService.page(page);
        return new JsonData().buildSuccess(menu, 200);
    }

    @ApiOperation("查询单个菜单")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "code") String code) {
        QueryWrapper queryWrapper = new QueryWrapper<Menu>();
        Menu menu = menuService.getOne(queryWrapper.eq(true, "menu_code", code));
        return new JsonData().buildSuccess(menu, 200);
    }

    @ApiOperation("新增菜单")
    @PostMapping(value = "/regist", consumes = "application/json")
    public Object regist(@RequestBody Menu menu) {
        boolean isSuccess = menuService.save(menu);
        return new JsonData().buildSuccess("" + isSuccess, 0);
    }

    @ApiOperation("修改单个菜单项")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个菜单")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "code") String code) {
        QueryWrapper queryWrapper = new QueryWrapper<Menu>();
        boolean isSuccess = menuService.remove(queryWrapper.eq(true, "menu_code", code));
        return new JsonData().buildSuccess("" + isSuccess, 0);
    }
}
