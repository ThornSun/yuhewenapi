package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hbut603.yuhewenapi.api.entity.Answer;
import com.hbut603.yuhewenapi.api.service.IAnswerService;
import com.hbut603.yuhewenapi.api.utils.CodeGenerator;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 回答表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/answer")
public class AnswerController {
    @Autowired
    private IAnswerService answerService;

    @ApiOperation("查询所有用户")
    @GetMapping("/findall")
    public Object findAll(
            @RequestParam("currentPage") Integer currentPage,
            @RequestParam("pageSize") Integer pageSize) {
        Page<Answer> page = new Page<>(currentPage, pageSize);
        IPage<Answer> answers = answerService.page(page.setDesc("answer_id"));
        return new JsonData().buildSuccess(answers, 200);
    }

    @ApiOperation("查询单个用户")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "A_id") String account) {
        QueryWrapper queryWrapper = new QueryWrapper<Answer>();
        Answer answer = answerService.getOne(queryWrapper.eq(true, "answer_account", account));
        return new JsonData().buildSuccess(answer, 200);
    }

    @ApiOperation("新增用户")
    @PostMapping(value = "/regist", consumes = "application/json")
    public Object regist(@RequestBody Answer answer) {
        answer.setAnswerCode(new CodeGenerator().codeGenerateCenter(4));
        boolean isSuccess = answerService.save(answer);
        return new JsonData().buildSuccess("" + isSuccess, 0);
    }

    @ApiOperation("修改单个用户信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个用户")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "A_id") String account) {
        QueryWrapper queryWrapper = new QueryWrapper<Answer>();
        boolean isSuccess = answerService.remove(queryWrapper.eq(true, "answer_account", account));
        return new JsonData().buildSuccess("" + isSuccess, 0);
    }

    @ApiOperation("按提问查询答案")
    @GetMapping("/findbyqid")
    public Object findByQid(@RequestParam(value = "qid") String qid) {
        QueryWrapper queryWrapper = new QueryWrapper<Answer>();
        List<Answer> answer = answerService.listObjs(queryWrapper.eq(true, "answer_question_id", qid));
        return new JsonData().buildSuccess(answer, 200);
    }

}

