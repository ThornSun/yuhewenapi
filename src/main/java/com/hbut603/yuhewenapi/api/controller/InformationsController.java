package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hbut603.yuhewenapi.api.entity.Informations;
import com.hbut603.yuhewenapi.api.service.IInformationsService;
import com.hbut603.yuhewenapi.api.utils.CodeGenerator;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 资讯与动态表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/informations")
public class InformationsController {
    @Autowired
    private IInformationsService informationsService;

    @ApiOperation("查询所有提问")
    @GetMapping("/findall")
    public Object findAll(
            @RequestParam("currentPage") Integer currentPage,
            @RequestParam("pageSize") Integer pageSize) {
        Page<Informations> page = new Page<>(currentPage,pageSize);
        IPage<Informations> informations = informationsService.page(page);
        return new JsonData().buildSuccess(informations, 200);
    }

    @ApiOperation("查询单个提问")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "I_id")  String id) {
        QueryWrapper queryWrapper = new QueryWrapper<Informations>();
        Informations information = informationsService.getOne(queryWrapper.eq(true,"info_id",id));
        return new JsonData().buildSuccess(information, 200);
    }

    @ApiOperation("新增提问")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody Informations information) {
        information.setInfoCode(new CodeGenerator().codeGenerateCenter(7));
        boolean isSuccess = informationsService.save(information);
        return new JsonData().buildSuccess(""+isSuccess,0);
    }

    @ApiOperation("修改单个提问信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个提问")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "I_id")  String id) {
        QueryWrapper queryWrapper = new QueryWrapper<Informations>();
        boolean isSuccess = informationsService.remove(queryWrapper.eq(true,"info_id",id));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }
    
    @ApiOperation("按种类查询动态")
    @GetMapping("/findbytype")
    public Object findByType(@RequestParam(value = "type")  String type) {
        QueryWrapper queryWrapper = new QueryWrapper<Informations>();
        List<Informations> information = informationsService.listObjs(queryWrapper.eq(true,"info_type",type));
        return new JsonData().buildSuccess(information, 200);
    }
}

