package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hbut603.yuhewenapi.api.entity.UserMark;
import com.hbut603.yuhewenapi.api.service.IUserMarkService;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * <p>
 * 成绩评测成绩表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/userMark")
public class UserMarkController {
    @Autowired
    private IUserMarkService userMarkService;

    @ApiOperation("查询所有成绩")
    @GetMapping("/findall")
    public Object findAll(
        @RequestParam("currentPage") Integer currentPage,
        @RequestParam("pageSize") Integer pageSize) {
            Page<UserMark> page = new Page<>(currentPage,pageSize);
            IPage<UserMark> userMark = userMarkService.page(page);
        return new JsonData().buildSuccess(userMark, 200);
    }

    @ApiOperation("查询单个成绩")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "UM_account")  String account) {
        QueryWrapper queryWrapper = new QueryWrapper<UserMark>();
        UserMark userMark = userMarkService.getOne(queryWrapper.eq(true,"mark_id",account));
        return new JsonData().buildSuccess(userMark, 200);
    }

    @ApiOperation("新增成绩")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody UserMark userMark) {
        boolean isSuccess = userMarkService.save(userMark);
        return new JsonData().buildSuccess(""+isSuccess,0);
    }

    @ApiOperation("修改单个成绩信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个成绩")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "UM_account")  String account) {
        QueryWrapper queryWrapper = new QueryWrapper<UserMark>();
        boolean isSuccess = userMarkService.remove(queryWrapper.eq(true,"mark_id",account));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }
}

