from docker.io/tomcat:latest
MAINTAINER ThornSun
ADD ./target/newhorizonAPI.war /usr/local/tomcat/webapps/
CMD ["catalina.sh", "run"]

